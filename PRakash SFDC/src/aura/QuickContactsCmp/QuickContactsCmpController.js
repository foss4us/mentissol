({
	doInit : function(cmp, event) {
        var action = cmp.get("c.findAll");
        action.setCallback(this, function(a){
           cmp.set("v.contacts", a.getReturnValue()); 
        });
        $A.enqueueAction(action);
	},
    searchKeyChange : function(cmp, event) {
        var searchKey = event.getParams("searchKey");
        var action = cmp.get("c.findByName");
        action.setCallback(this, function(a){
            action.setParams({"searchKey":searchKey});
           cmp.set("v.contacts", a.getReturnValue()); 
        });
        $A.enqueueAction(action);
	}
})