({
	echo : function(cmp) {
        var action = cmp.get("c.SeverEcho");
        // Use action.setParams() 
        // to set the arguments to be passed to the server-side controller.
        action.setParams({
            firstName : cmp.get("v.firstName")
        });
        //Create a Callback using action.setCallback(); that is invoked after the 
        // server-side action returns.
        action.setCallback(this, function(response){
            var state = response.getState();
            // The callback dosen't reference cmp. If it did
            // you should run isValid() to check 
            // if(isValid() && state==="SUCCESS")
            if(state==="SUCCESS"){
                alert("From Server:"+response.getReturnValue());
            }
            else if(state==="INCOMPLETE"){
                // DO something
            }
                else if(state==="ERROR"){
                    var Errors = response.getError();
                    if(Errors){
                        if(Errors[0] && Errors[0].message){
                            console.log("Error Message: " +response.Errors[0].message);
                        }
                    }
                    else{
                        console.log("Unknown Error");
                    }
                }
        });
        //$A. enquequeAction adds the server side action to the queue.
        $A.enqueueAction(action);
	}
})