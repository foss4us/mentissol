trigger Filter on ContentVersion (after insert) {
       for (ContentVersion cv : Trigger.new) {
        // Origin is 'H' for Chatter files, 'C' for Content documents
        // Leave out the test for Origin if you want to cover both
        if (cv.Origin == 'H' && cv.FileType != 'PDF') {
            cv.addError('Only PDF files are allowed!');
        }
    }
}