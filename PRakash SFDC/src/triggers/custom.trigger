trigger custom on Contact (before insert, before update) {
    set<Id> cId = new set<Id>();
    if(trigger.isInsert || trigger.isupdate){
        for(Contact con : trigger.new){
            if(con.accountId != NULL){
                cId.add(con.accountId);
            }
        }  
        
        
     List<Account> acclist = [select Name, Id, Equipment_order__c,(
        select Id, List_type__c, accountId from Contacts) from Account where Id =:cId];
        for(Account a : acclist){
            for(Contact c : trigger.new){
                if((a.Equipment_order__c =='Return to central' && c.List_type__c != 'Assign')||
                   (a.Equipment_order__c=='Trunk stock' && c.List_type__c!='Request')||
                   (a.Equipment_order__c=='Demonstration'&& c.List_type__c!='Reservation')){
                    c.addError('Please  check the data entered');
                }
            }
        }
    }
}