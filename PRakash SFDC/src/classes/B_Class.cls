global class B_Class implements Database.Batchable<sObject>{

    global final string query;
    global final string s_object;
    global final string Field;
    global final string Field_value;
   
    public B_Class(String q, String s, String f, String v){
        query = q;
        s_object = s;
        Field = f;
        Field_value = v;  
    }
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<sObject> batch){
        for(sObject s : batch){
            s.put(Field, Field_value);
        }
        update batch;
    }
    public void finish(Database.BatchableContext BC){
        List<Contact> clist = new List<Contact>();
        clist = [Select Id, email, accountId from Contact where accountId = '0014100000BWPj6'];
        List<Id> con = new List<Id>();
        for(Contact c : clist){
            con.add(c.Id);
        } 
        Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
        mail.setTargetObjectIds(con);
        mail.setTemplateId('00X410000014dQG');
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });
    }      
    
}