global class StatefulBatch implements database.Batchable<sobject>, database.stateful{
    global string query;
    global integer count = 0;
    global list<account> accList = new list<account> ();
    
    global database.QueryLocator start(database.BatchableContext BC){
     String query = 'select Name, Id, Active__c,phone from Account';
        return database.getQueryLocator(query);
    }
    global void execute(database.BatchableContext BC, List<Account> scope){
       
        for(Account a : scope){
            if(a.Active__c == 'Yes')
            {
                a.Active__c = 'No';
                a.phone = '143';

            }
            
           accList.add(a);
            count++;
        }
        update accList;
    }
    global void finish(database.BatchableContext BC){
        system.debug('Account Info:' +count);  
                system.debug('Updated Records :' +accList);
    }
}