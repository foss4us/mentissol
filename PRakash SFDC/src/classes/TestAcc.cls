@isTest
public class TestAcc {
    public static testmethod void Check(){
        List<Account> acc = new List<Account>();
        Account a1 = new Account(Name='Prakash', Description='Updated By prakash');
        Account a2 = new Account(Name='Prakash1', Description='Updated By prakash');
        Account a3 = new Account(Name='Prakash2', Description='Updated By prakash');
        acc.add(a1);
        acc.add(a2);
        acc.add(a3);
        insert acc;
        List<Account> query = [select Id, Name,Description from Account];
        List<Account> acclist = new List<Account>();
        for(Account a : query){
            a.Description = 'Updated by me';  
            acclist.add(a);
        }
          
        update acclist;
        test.startTest();
        Bat_Class bat = new Bat_Class();
        database.executeBatch(bat);
        test.stopTest();
    }
}