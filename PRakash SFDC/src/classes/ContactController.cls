public with sharing class ContactController {
    @AuraEnabled
    public static List<Contact> findAll(){
        return [Select Id, Name, Phone from Contact  LIMIT 50];
    }
    @AuraEnabled
    public static List<Contact> findByName(String searchKey){
        String name = '%' +searchKey+ '%';
        return [Select Name, Id, Phone from Contact where Name LIKE:name LIMIT 50];
    }
    @AuraEnabled
    public static List<Contact> findById(String contactId){
        return [Select Name, Id, Phone, title, Account.Name from Contact where Id=:contactId LIMIT 50];
    }
}