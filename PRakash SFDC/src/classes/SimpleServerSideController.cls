public class SimpleServerSideController {
    // Use @AuraEnabled to access Client- and server-side controller
    @AuraEnabled
    public static string SeverEcho(string firstName){
        return('Hello! This is response from the server,' +firstName);
    }
}