public with sharing class LoginClass{
    public String password { get; set; }
    public String username { get; set; }
    
    public PageReference register() {
        List<Login__c> obj = new List<Login__c>();
        if(username!=null && username!=''||password!=null && password!=''){
            obj = [Select Name,Password__c from Login__c where Name =:username Limit 1];
            if(obj.size()>0){
                for(Login__c log : obj){
                    if(password == log.Password__c){
                        return Page.DisplayVendor;
                    }
                    else{
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Invalid Password' );
                        ApexPages.addMessage(myMsg);
                        system.debug('MyMsg:'+myMsg);
                    }
                }
            }
        }
        
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Username or Password does not exist' );
            ApexPages.addMessage(myMsg);
            system.debug('MyMsg:'+myMsg);
        }
        return null;
    }
    public PageReference forget(){
    return Page.Forget;
    }
}