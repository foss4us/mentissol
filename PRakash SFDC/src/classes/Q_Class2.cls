public class Q_Class2 implements Queueable{
    public void execute(QueueableContext con){
        Account a = [select Name, Id from Account where Name = 'Queueable2'];
        Contact c = new Contact(LastName='Queue', accountId=a.Id);
        insert c;
    }
}