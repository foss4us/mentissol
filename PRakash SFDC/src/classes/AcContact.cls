@isTest
private class AcContact {
	
	
	static testMethod void unitTestVisualforceExtension() {
		Account a = [SELECT Id FROM Account]; 
        
		Test.startTest(); //denote testing context
		PageReference pageRef = Page.AccContact; //create a page reference to yourVisualforcePage.page
		Test.setCurrentPage(pageRef); //set page context
		//test the methods from your controller...
		Test.stopTest(); //revert from testing context
	}
	
	//illustrates how to build more data into your unit test
	static testMethod void unitTestCreateMoreData() {
		List<Contact> contacts = [SELECT Id FROM Contact]; //grab the Contacts created in the allTheDataForThisTestClass method
		//create a Campaign
		List<Campaign> campaigns = new List<Campaign>();
		campaigns.add(new Campaign(Name = 'Unit Testing for Today Campaign', Description = 'This is for testing purposes only!'));
		insert campaigns;
		//create CampaignMembers
		List<CampaignMember> campaignmembers = new List<CampaignMember>(); //list for new CampaignMembers
		for (Contact c : contacts) { //for all contact records we already inserted
			campaignmembers.add(new CampaignMember(CampaignId = campaigns[0].Id, ContactId = c.Id, Status = 'Sent')); //create a new CampaignMember
		}
		insert campaignmembers; //insert the CampaignMembers
		Test.startTest(); //denote testing context
		//do your testing...
		Test.stopTest(); //revert from testing context
	}
	
	//testing data setup for all methods in this class
	@testSetup
	static void allTheDataForThisTestClass() {
		//create accounts
		List<Account> accounts = new List<Account>();
		accounts.add(new Account(Name = 'Unit Test Corp'));
		accounts.add(new Account(Name = 'Unit Test LLC'));
		accounts.add(new Account(Name = 'Parent Company Inc'));
		insert accounts;
		//update accounts in order to create a relationship
		List<Account> accountUpdates = new List<Account>();
		accountUpdates.add(new Account(Id = accounts[0].Id, ParentId = accounts[1].Id));
		accountUpdates.add(new Account(Id = accounts[1].Id, ParentId = accounts[2].Id));
		update accountUpdates;
		//create some contacts
		List<Contact> contacts = new List<Contact>();
		contacts.add(new Contact(AccountId = accounts[0].Id, FirstName = 'Tess', LastName = 'Dachshund'));
		contacts.add(new Contact(AccountId = accounts[0].Id, FirstName = 'Grachus', LastName = 'Dachshund'));
		contacts.add(new Contact(AccountId = accounts[1].Id, FirstName = 'Pete', LastName = 'Dachshund'));
		contacts.add(new Contact(AccountId = accounts[1].Id, FirstName = 'Alphonse', LastName = 'Dachshund'));
		insert contacts;
		//create some Opportunities
		List<Opportunity> opportunities = new List<Opportunity>();
		opportunities.add(new Opportunity(AccountId = accounts[0].Id, CloseDate = Date.Today().addMonths(1), Name = 'Testing Opportunity - This is a truck', StageName = '1. Identified Opportunity'));
		opportunities.add(new Opportunity(AccountId = accounts[1].Id, CloseDate = Date.Today().addMonths(1), Name = 'Testing Opportunity - This is a weasel', StageName = '1. Identified Opportunity'));
		insert opportunities;
	}

}