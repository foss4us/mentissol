public with sharing class AccountIns {
    public void accInsert(){
        Account acc = new Account();
        acc.Name = 'PrakashAsha';
        acc.Industry = 'Energy';
        acc.Active__c = 'Yes';
        insert acc;
        system.debug(acc);
    }
}