@isTest(SeeAllData=true)
public class TestStatefulBatch {
    public static testmethod void Check(){
        Account query = [select Name, Id, Active__c, phone from Account where Name = 'Test10'];
        query.Active__c = 'yes';
        query.Phone = '142';
        update query;
        test.startTest();
        StatefulBatch instance = new StatefulBatch();
        database.executeBatch(instance);
        test.stopTest();
    }
}