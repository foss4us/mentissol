global class Bat_Class implements Database.Batchable<sObject>{
    global List<Account> acc = new List<Account>();
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'Select Id, Name from Account';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        for(Account a : scope){
            a.Description = 'Updated By Ashaprakash';
            acc.add(a);
        }
        update acc;
    }
    global void finish(Database.BatchableContext BC){
        system.debug('Account Info'+acc);
    }
}