global class Batch_Class implements Database.Batchable<sObject>{
    global final string query;
    global final string s_object;
    global final string Field;
    global final string Field_value;
    global final string email;
    
    public Batch_Class(String q, String s, String f, String v, String e){
        query = q;
        s_object = s;
        Field = f;
        Field_value = v;
        email = e;
    }
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<sObject> batch){
        for(sObject s : batch){
            s.put(Field, Field_value);
        }
        update batch;
    }
    public void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new string[] {email});
        mail.setReplyTo('prakash094u@gmail.com');
        mail.setSubject('Welcome to Batch Apex');
        mail.setHtmlBody('My Dear Wife, <br /><br /> Hello! welcome to salesforce Batch Apex Class<br /><br /> Date :'+string.valueof(system.today()));
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}

/*
Execution process:

ctrl+E

String q = 'select Description from Account limit 10';
String s = 'Account';
String f = 'Description';
String v = 'Updated by Asha';
string e = 'nalluriasha.76@gmail.com';
Id batchinstanceId = Database.executeBatch(new Batch_Class(q, s, f, v, e));

*/