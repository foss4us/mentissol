public with sharing class ResetClass {

    public PageReference change() {
         List<Login__c> obj = new List<Login__c>();
         obj = [Select Name, Password__c from Login__c where Password__c =:password Limit 1];
            for(Login__c log : obj){
                    if(password==log.Password__c){
                             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Password Should be New' );
                            ApexPages.addMessage(myMsg);
                            system.debug('MyMsg:'+myMsg);                       
                        }
                        else if(password!=cpassword){
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Password Missmatched' );
                            ApexPages.addMessage(myMsg);
                            system.debug('MyMsg:'+myMsg);
                        }
                        else{
                        Login__c logs = new Login__c();
                            logs.Password__c= cpassword;
                            
                            upsert logs;
                            return Page.Login;
                        }
                        
                    }
             return null;

        }

        public PageReference login() {
        return Page.Login;
        }
    
        public String cpassword { get; set; }

        public String password { get; set; }
}