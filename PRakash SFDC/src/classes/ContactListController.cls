public class ContactListController {
    @AuraEnabled
    public static List<Contact> getContacts(){
        return [SELECT Id, Name, Email, Phone FROM Contact 
                where Phone != NULL];
    }
}