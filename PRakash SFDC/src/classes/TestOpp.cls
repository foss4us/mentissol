@isTest
public class TestOpp {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    public static testmethod void check(){
        Account a = new Account(Name='Schedule class');
        insert a;
        test.startTest();
        S_Class schedule = new S_Class();
        system.schedule('Test check', CRON_EXP, schedule);
        test.stopTest();
    }
}