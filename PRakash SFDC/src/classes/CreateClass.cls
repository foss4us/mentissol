public with sharing class CreateClass {

    public String domain { get; set; }

    public String recruter { get; set; }
    
    public String vendor { get; set; }

    public PageReference logout() {
        return Page.Login;
    }


    public PageReference create() {
    List<Vendor__c> ven = new List<Vendor__c>();
    if(vendor!=null&&vendor!=''||recruter!=null&&recruter!=''||domain!=null||domain!=''){
        Vendor__c vend = new Vendor__c();
        
        vend.Vendor_Company__c=vendor;
        vend.Recruter__c = recruter;
        vend.Company_Domain__c = domain;
        
        ven.add(vend);
        insert ven;
        return Page.Login;
    }else{
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Fields must be filled' );
        ApexPages.addMessage(myMsg);
        system.debug('MyMsg:'+myMsg);
    }
        return null;
    }


    
}