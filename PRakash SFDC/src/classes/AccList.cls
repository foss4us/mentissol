public class AccList {
    public List<Account> accList = new List<Account>();
    public void display(){
        Account a1 = new Account(Name='Test10', Active__c='No');
        Account a2 = new Account(Name='Test100', Active__c='No');
        Account a3 = new Account(Name='Test1000', Active__c='No');
        Account a4 = new Account(Name='Test10000', Active__c='No');
        Account a5 = new Account(Name='Test100000', Active__c='No');
        accList.add(a1);
        accList.add(a2);
        accList.add(a3);
        accList.add(a4);
        accList.add(a5);
        insert accList;
    }
}