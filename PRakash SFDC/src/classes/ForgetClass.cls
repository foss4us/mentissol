public with sharing class ForgetClass {
   public String username { get; set; }
   public String apiSessionId {get; set;}
   public PageReference forget() {
   System.debug('apiSessionId: ' + apiSessionId); 
        List<Login__c> obj = new List<Login__c>();
        obj = [Select Name from Login__c where Name =:username Limit 1];
        if(obj.size()>0){
            for(Login__c log : obj){
                    if(username==log.Name){
                        return Page.Reset;
                        }
                        
                    }
                }
                else{
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Invalid User Name' );
                            ApexPages.addMessage(myMsg);
                            system.debug('MyMsg:'+myMsg);
                        }
             return null;
   }
}