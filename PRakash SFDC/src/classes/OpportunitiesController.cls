public with sharing class OpportunitiesController {
    @AuraEnabled
    public static List<Opportunity> getOpportunities(){
        List<opportunity> opportunities = [select Id, Name from Opportunity];
        return opportunities;
    }
}