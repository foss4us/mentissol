global class Scheduler_class implements Schedulable{
    public static String CRON_EXP = '0 0 0 3 9 ? 2022';
    global void execute(SchedulableContext sc) {
        Bat_Class b1 = new Bat_Class();
        ID batchprocessid = Database.executeBatch(b1);           
    }
}